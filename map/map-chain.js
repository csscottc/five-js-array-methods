const bears = [
    {"name": "Baloo",
     "type": "Brown"},
    {"name": "Yogi",
     "type": "Brown"},
    {"name": "Po",
     "type": "Panda"},
    {"name": "Bungle",
     "type": "Brown"}
];

function teddify(bear) {
    return {
        "name": bear.name,
        "type": "Teddy",
        "price": "5.00"
    }
}

function printOut(bear) {
    console.log(JSON.stringify(bear));
}

bears.forEach((bear) => printOut(bear));
bears.map((bear) => teddify(bear)).forEach((teddy) => printOut(teddy));