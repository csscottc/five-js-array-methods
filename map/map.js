const bears = [
    {"name": "Baloo",
     "type": "Brown"},
    {"name": "Yogi",
     "type": "Brown"},
    {"name": "Po",
     "type": "Panda"},
    {"name": "Bungle",
     "type": "Brown"}
];

let teddyBears = bears.map((bear) => {
    return {
        "name": bear.name,
        "type": "Teddy",
        "price": "5.00"
    }
});

bears.forEach((b) => {
    console.log(JSON.stringify(b));
})

teddyBears.forEach((tb) => {
    console.log(JSON.stringify(tb));
});
