const bears = [
    {"name": "Baloo",
     "type": "Brown"},
    {"name": "Yogi",
     "type": "Brown"},
    {"name": "Po",
     "type": "Panda"},
    {"name": "Bungle",
     "type": "Brown"}
];

// Rather than return a new teddy bear, we will just mutate the existing bear
function teddify(bear) {
    bear.type = "Teddy";
    bear.price = "5.00";
}

// We will use forEach instead of map
bears.forEach((bear) => teddify(bear));
bears.forEach((bear) => console.log(JSON.stringify(bear)));