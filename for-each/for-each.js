const bears = ["Yogi", "Baloo", "Po", "Winnie", "Bungle"];

function greet(bear) {
    console.log(`Hello there, ${bear}!`);
}

bears.forEach((bear) => {
    greet(bear);
});
