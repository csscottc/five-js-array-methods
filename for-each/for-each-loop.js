const bears = ["Yogi", "Baloo", "Po", "Winnie", "Bungle"];

function greet(bear) {
    console.log(`Hello there, ${bear}!`);
}

for (let i = 0; i < bears.length; i++) {
    greet(bears[i]);
}
